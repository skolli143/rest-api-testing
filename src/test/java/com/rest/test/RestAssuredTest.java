package com.rest.test;

import io.restassured.response.Response;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class RestAssuredTest {

    @Test
    public void getRequest(){

        given().
                when().get("<PUT GET ENDPOINT>").
                then().statusCode(200);
    }

    @Test
    public String getResponseAsString(){

       Response response=  given().
                when().get("<PUT GET ENDPOINT>");

       return response.getBody().asString();

    }

    @Test
    public void postRequest(){
        given().
                header("Content-Type","application/json").
                header("Authorization", "Bearer "+getBearerToken()).
                when().body("<PUT body string>").
                post("<PUT POST ENDPOINT>").
                //TODO put appropriate status code
                then().statusCode(201);
    }

    @Test
    public void putRequest(){
        given().
                header("Content-Type","application/json").
                header("Authorization", "Bearer "+getBearerToken()).
                when().body("<PUT body string>").
                put("<PUT ENDPOINT>").
                //TODO put appropriate status code
                        then().statusCode(201);
    }

    @Test
    public void deleteRequest(){
        given().
                header("Content-Type","application/json").
                header("Authorization", "Bearer "+getBearerToken()).
                when().
                delete("<PUT ENDPOINT>").
                //TODO put appropriate status code
                        then().statusCode(201);
    }

    public String getBearerToken(){
        String token =null;
        //TODO put bearer token method
        return token;
    }
}
